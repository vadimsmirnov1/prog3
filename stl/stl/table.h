#include "StdAfx.h"
#include <iomanip>

template <class MyType1>
 class iterator1 {
	
private:
	MyType1 * cur;
public:
	
	iterator1(MyType1* f = NULL ){
		cur = returnptr(f);
	}
	MyType1* returnptr(MyType1* f){return f;}
	//MyType1* end(MyType1* f){return f;}
	iterator1 & operator ++(){
		cur = cur+1;
		return *this;
	}

int operator !=(iterator1 &it){
	return cur != it.cur;
}
bool operator ==( iterator1 b){
	return (cur == b.getcur());
}


friend std::ostream& operator <<(std::ostream & os,const iterator1 & r){
		os<<*r.cur;
	return os;
};
MyType1* getcur(){return cur;};
	~iterator1(){};
};



template <class MyType>
class Vector {
private:
int top;
MyType* arr;
public:
	Vector(){top = 0;arr = NULL;};
	int returntop(){return top;};
int pushback(MyType & el){
	MyType* a = new MyType[top+1];
	for(int i =0 ; i<top;i++)
		a[i] = std::move(arr[i]);
		//a[i] = arr[i];
	if(top)
		delete [] arr;
	a[top]= el;
	arr = a;
	top++;
	return 0;
}
void sort(int l,int r){
	if (arr == NULL)
		return;
int i,j,y;
MyType x;
    i=l; j=r; 
	int k;
	k= ((r+l)/2);
	x = arr[k];
    do{
      while (arr[i]<x)
			i++;
      while (x<arr[j]) 
			j--; 
      if (i<=j) {
        if (arr[i] > arr[j]){ 
			MyType R;
			R = arr[j];
			arr[j] = arr[i];
			arr[i] = R;
			/*room* l;
			 y=aprt[i].num;
			 l = aprt[i].rm;
			 aprt[i].num=aprt[j].num;
			 aprt[i].rm=aprt[j].rm;
			 aprt[j].num=y;
			 aprt[j].rm=l*/
		}
       
        i++;
		j--;
	  }
	}while(!(i>=j));
    if (l<j) 
		sort(l,j);
    if (i<r) 
		sort(i,r);

  }

int gettop(){
	return top;
};

template <class MyType1>
friend class  iterator1;
//typedef iterator1 <MyType> Iterator;
iterator1 <MyType> begin(){
	iterator1 <MyType> a(arr);
	return a.returnptr(arr);
};
iterator1 <MyType> end(){
	iterator1 <MyType> a(arr);
	return a.returnptr(arr+top);
};
template <class Z>
iterator1<MyType> find(Z k){
	for(auto i= this->begin();i!= this->end();i++){
		if (i == k)
			return i;
	}
	return this->end();
};
~Vector(){delete[] arr;};
};
