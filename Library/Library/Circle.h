

#pragma once
#include "StdAfx.h"
#include <iostream>

#include <string>

#include <math.h>
#define PI 3.14159265358979323846264

#include <stdlib.h>
using namespace std;
#include"..\Library\table.h"








/*!
Abstruct class
\author 111 
	\version 1.000006
	\date 01.02.0003
	\warning DONT use!!1
*/
class room{
protected:

	virtual std::ostream & print(std::ostream & ) const= 0;
	/*! set IOS state on failbit if  smth wrong */
	virtual std::istream& enter(std::istream &) = 0;
public:
	/// "overload" output
	friend std::ostream& operator <<(std::ostream & os,const room & r){
		return r.print(os);
	};
	/// "overload" input
	friend std::istream& operator >>(std::istream & is,room & r){
		return r.enter(is);
	};
	/// show vacant or occupied \return 0 if freee
	virtual int getfree() const = 0;
	/// make room free \return payment
	virtual int makefree() = 0;

	virtual ~room(){}
};

 
/*! Just struct for date 
*/
struct strdate{
	int day,
		month,
		year;

};
///Single room
class one:public room{
	
protected:
	/// free (0) or occupied (!0)
	int free;
	strdate date;
	///the number of days of living
	int N;
	/// mooooooney per day
	int tarif;
	///output single room
	 std::ostream & print(std::ostream & c) const;
	 ///input single room
	 	/*! set IOS state on failbit if  smth wrong */
	 std::istream & enter(std::istream & s);
public:
	///default constructor; all statements = 0
	one():free(0),N(0),tarif(0){
		date.day = 0;
		date.month = 0;
		date.year = 0;
	
	}
	///nondefault constructor
	/*!
	free1 = freely of room;
	date1 = date of registration;
	N1 = nubers of days of living;
	tarif1 = tarif for 1 day;
	*/
	one(int free1,strdate date1,int N1, int tarif1){
		free = free1;
		date.day = date1.day;
		date.month  = date1.month;
		date.year = date1.year;
		N = N1;
		tarif = tarif1;
	}
	/// \returns the number of days of living 
	int getN(){
		return N;
	}
	virtual	int makefree(){ 
		if(free == 0){
			cout<<"Room is frreeeeee!!12"<<endl;
			return 0;
		}
		int h = N*tarif;
		free = 0;
		date.day = 0;
		date.month  = 0;
		date.year = 0;
		N = 0;
		tarif = 0;
		return h;
	}

	virtual	int getfree() const{ return free;}

	~one(){};
};


/// Lux room
class lux:public one{
private :
	/// the number of living people
		int Nl,
			/// the number of rooms in Lux
			Nr;
protected:
	///input Lux room
	std::ostream & print(std::ostream & c) const;
	///input Lux room
	/*! set IOS state on failbit if  smth wrong */
	std::istream & enter(std::istream & s);
public:
	///default/nondefault constructor
	lux(int Nl1 = 0,int Nr1 = 0):one(){
		Nl = Nl1;
		Nr = Nr1;
	}

	virtual	int getfree() const{ return free;}
	virtual	int makefree(){ 
		if (free == 0){
			cout<<"Room is frreeeeee!!12"<<endl;
			return 0;
		}
		Nl = 0;

		return one::makefree();
	}
	/// settling in Lux - change the statement of freely Lux on unfreely room
	void enterr();
	///the number of living people
	int getNl() { return Nl;}
	~lux(){}
};
///multiplace room 
class lot:public room{
private:
	/// the number of places in room
	int Np,
		/// the number of living people(1 man = 1 place)
		Nl;
	one* m; 
protected:
	///output lot room
	std::ostream & print(std::ostream & c) const;
	///input lot room
	/*! set IOS state on failbit if  smth wrong */
	std::istream & enter(std::istream & s);
public:
	///default/nondefault constructor 
	lot(int Np1 = 0, int Nl1 = 0){
		Np = Np1;
		Nl = Nl1;
		m = new one [Nl];
	}


	
	virtual	int getfree() const{ 
		if (Np - Nl)
			return 0;
			return 1;
	}
	virtual	int makefree();
	/// \return the number of living people
		int getNl()	{ return Nl;}
		
	/// make freely place occupied
	 void enterplace();
	~lot(){
		if (Nl!=0)
			delete [] m;
	}
};










	













		/*
		friend std::ostream & operator <<(std::ostream &c,polygon &h){
			c<<"N = "<<h.N<<endl;
			for(int i = 0;i<h.N;i++){
				c<<i+1<<" ("<<h.mass[i].x<<","<<h.mass[i].y<<")"<<endl;
			}
			return c;
		}

		friend istream & operator >>(istream & s,polygon &r){
			r.myrealloc(1);
			s>>r.mass[r.getN()].x>>	
				r.mass[r.getN()].y;
			
			if(s.good()){
				r.N+=1;
				return s;
			}
			s.setstate(std::ios::failbit);
			return s;
		
		}
		*/







	
